import { AppDispatcher } from './app';

import { NestFactory } from '@nestjs/core';
async function bootstrap() {
  await NestFactory.create(AppDispatcher);
}
bootstrap();

