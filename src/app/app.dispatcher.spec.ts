import { AppDispatcher } from './app.dispatcher';
import { AppLoggerService } from './helpers';
import { TestingModule, Test } from '@nestjs/testing';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

describe('Dispatchers', () => {
    let appDispatcher: AppDispatcher;
    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [AppDispatcher,
            ],
        }).compile();

        appDispatcher = module.get<AppDispatcher>(AppDispatcher);
    });

    it('should be defined', () => {
        const app = NestFactory.createApplicationContext(AppModule);
        jest.spyOn(appDispatcher, 'getContext').mockImplementation(() => app ) ;
        jest.spyOn(appDispatcher, 'dispatch');
        expect(appDispatcher).toBeDefined();
       // expect(appDispatcher.dispatch).toReturn();
    });

});

// describe("App Dispatchers", () => {
//     const logger = new AppLoggerService({ appPath: process.cwd() }, 'Index');
//     it('should dispatch the app', async () => {
//         const logger = new AppLoggerService({ appPath: process.cwd() }, 'Index');

//         let message = {
//             Title: 'Application Start',
//             Type: 'Info',
//             Detail: 'Application Start',
//             Status: 'Status',
//         };

//         logger.log(message);

//         const dispatcher = new AppDispatcher();
//         dispatcher
//             .dispatch()
//             .then(() =>
//                 logger.log({
//                     Title: 'Application Up',
//                     Type: 'Info',
//                     Detail: 'pplication Up',
//                     Status: 'Status',
//                 }),

//             )
//             .catch(e => {
//                 logger.error(e.message, e.stack);
//                 process.exit(1);
//             });

//     });
// });

    // it('should be defined', () => {
    //     const app = NestFactory.createApplicationContext(AppModule);
    //     jest.spyOn(appDispatcher, 'getContext').mockImplementation(() =>{ return app} ) ;
    //     jest.spyOn(appDispatcher, 'dispatch');
    //     expect(appDispatcher).toBeDefined();
    //     expect(appDispatcher.getContext()).toEqual(app);
    //     expect(appDispatcher.dispatch).toHaveBeenCalled();
    //    // expect(appDispatcher.dispatch).toReturn();
    // });

// })
