import { AppModule } from './app.module';
import { TestingModule, Test } from '@nestjs/testing';
import { AppLoggerService, LOGGER_MODULE_OPTIONS } from './helpers';

describe('App Module', () => {
  let appModule: AppModule;

  const loggerOptions = { appPath: process.cwd() };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AppModule,
        {
          provide: AppLoggerService,
          useClass: AppLoggerService,
        },
        {
          provide: LOGGER_MODULE_OPTIONS,
          useValue: loggerOptions,
        },
        // {
        //   provide: AppLoggerService,
        //   useValue: new AppLoggerService(loggerOptions, 'AppModule'),
        // },
      ],
    }).compile();

    appModule = module.get<AppModule>(AppModule);
  });

  it('should be defined', () => {
    expect(appModule).toBeDefined();
  });
});
