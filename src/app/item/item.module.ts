import { Module } from '@nestjs/common';
import { ItemController } from './controller/item.controller';
import { ItemService } from './services/item.service';
import { ShipperlogEntity, TestMasterEntity } from './dto';



@Module({
  controllers: [ItemController],
  providers: [ItemService, ShipperlogEntity, TestMasterEntity],
  exports:[ShipperlogEntity,TestMasterEntity]
})
export class ItemModule {}
