import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { RequestTimeout } from './../../helpers/';
import { ItemDto, TaskEntity } from '../dto';
import { ItemService } from '../services/item.service';
import { MessageEntity } from '../dto/messageEntity';

@Controller('item')
export class ItemController {
  private start: number;

  constructor(
    private readonly itemService: ItemService,
  ) {
    this.start = Date.now();
  }
  @Post('create')
  create(@Body() itemDto: ItemDto) {
    return this.itemService.addItem(itemDto);
  }
  @RequestTimeout("query")
  @Get(':code')
  async findOne(@Param('code') code: string) {
    return await this.itemService.getItems(code);
    //.then(resolve => {    return resolve;    });
  }
  @RequestTimeout("query")
  @Put('fetchOracleQueryData')
  async fetchOracleQueryData() {
    return this.itemService.fetchOracleQueryData();
  }
  @RequestTimeout("query")
  @Put('fetchOracleStoredProcData')
  async fetchOracleStoredProcData(@Body() taskEntity: TaskEntity) {
    return this.itemService.fetchOracleStoredProcData(taskEntity);
  }

  @RequestTimeout("query")
  @Put('fetchPGStoredProcData')
  async fetchPGStoredProcData() {
    return this.itemService.fetchPGStoredProcData();
  }
  @RequestTimeout("query")
  @Put('fetchPGQueryData')
  async fetchPGQueryData() {
    return this.itemService.fetchPGQueryData();
  }
  @Put('update')
  update(@Body() itemDto: ItemDto) {
    return this.itemService.updateItem(itemDto);
  }
  @Put('publish')
  publish(@Body() messageEntity: MessageEntity) {
    return this.itemService.publish(messageEntity);
  }
  @Put('publishTopic')
  publishTopic(@Body() messageEntity: MessageEntity) {
    return this.itemService.publishTopic(messageEntity);
  }
  @Put('publishDirect')
  publishDirect(@Body() messageEntity: MessageEntity) {
    return this.itemService.publishDirect(messageEntity);
  }
  @Put('newTask')
  newTask(@Body() messageEntity: MessageEntity) {
    return this.itemService.newTask(messageEntity);
  }
  @Delete(':code')
  remove(@Param('code') code: string) {
    return this.itemService.deleteItem(code);
  }
}
