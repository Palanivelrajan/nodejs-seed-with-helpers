import { Test, TestingModule } from '@nestjs/testing';
import { AppLoggerService, EmitDirectService, EmitTopicService, EmitService, NewTaskService, ConfigService, ConfigModule, DependencyUtlilizationService } from './../../helpers';
import { DomainApiService, PostgressDBService,OracleDBService } from './../../helpers';
import { HttpService } from '@nestjs/common';
import { ItemController } from '../controller/item.controller';
import { ItemService } from '../services/item.service';

describe('Item Controller', () => {
  let controller: ItemController;
  const option = {
    filterOptions: { id: '1' },
    loggerOptions: { appPath: process.cwd() },
    domainAPIOptions: { host: 'http://localhost', port: 3001 },
    healthCheckOptions: {
      id: '1',
      dnsList: [{ key: 'domain_api', url: 'http://localhost:3001' }],
    },
    oracleDBOptions: [
      {
        name: 'rfdest',
        user: 'rfdest',
        password: 'rfdest',
        connectString: 'alsyntstb.ohlogistics.com:1521/syntstb',
      },
      {
        name: 'DOCGENDEV',
        user: 'DOCGENDEV',
        password: 'DocGen$dev18',
        connectString: 'alsynwebd.ohlogistics.com/synwebd_maint',
      },
    ],
    postgressDBOptions: [
      {
        name: 'shipperanalyticsq',
        user: 'usr_shipper',
        host: 'FDC00pgsp305L.ohlogistics.com',
        database: 'shipperanalyticsq',
        password: 'Sh1pP3Rq@',
        port: 5432,
      },
    ],
    rabbitMQOptions: [
      {
        name: 'synapsebffcore',
        key: 'info',
        severity: 'severity',
        exchangeName: 'DirectX',
        exchangeType: 'Direct',
        connection: 'amqp://guest:guest@localhost:5672/?prefetch-count=1',
      },
      {
        name: 'synapsebffcoreDirect',
        key: 'info',
        severity: 'severity',
        exchangeName: 'DirectX',
        exchangeType: 'Direct',
        connection: 'amqp://guest:guest@localhost:5672/?prefetch-count=1',
      },
      {
        name: 'synapsebffcoreTopic',
        key: 'info',
        severity: 'severity',
        exchangeName: 'TopicX',
        exchangeType: 'Topic',
        connection: 'amqp://guest:guest@localhost:5672/?prefetch-count=1',
      },
    ],
  }

  // const domainAPIOption = { host: 'http://localhost/', port: 3001 };
  // const apploggerService = new AppLoggerService(  );

  // let dependencyUtlilizationService: DependencyUtlilizationService = new DependencyUtlilizationService();

  // const domainAPIServie = new DomainApiService(
  //   domainAPIOption,
  //   new HttpService(),
  //   dependencyUtlilizationService,
  //   apploggerService

  // );
  // const oraclDBService = new OracleDBService(option.oracleDBOptions);
  // const postgressDBService = new PostgressDBService(option.postgressDBOptions);
  // const emitService = new EmitService(option.rabbitMQOptions);
  // const emitDirectService = new EmitDirectService(option.rabbitMQOptions);
  // const emitTopicService = new EmitTopicService(option.rabbitMQOptions);
  // const newTaskService = new NewTaskService(option.rabbitMQOptions);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/src/config' })],
      controllers: [ItemController],
      providers: [
        AppLoggerService,ItemService,
        OracleDBService, PostgressDBService,
        DomainApiService, AppLoggerService, 
        EmitService, EmitDirectService, 
        EmitTopicService, NewTaskService, 
        HttpService, DependencyUtlilizationService],
    }).compile();

    controller = module.get<ItemController>(ItemController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
