import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {
  DomainApiService,
  OracleDBService,
  PostgressDBService,
  EmitService,
  EmitDirectService,
  EmitTopicService,
  NewTaskService,
} from './../../helpers';

import {
  ItemDto,
  ShipperlogEntity,
  TestMasterEntity,
  TaskEntity,
  MessageEntity,
} from '../dto';

import { Pool, Client } from 'pg';
const oracledb = require('oracledb');

@Injectable()
export class ItemService {
  constructor(
    private oracleDBervice: OracleDBService,
    private postgressDBService: PostgressDBService,
    private apiService: DomainApiService,
    private emitService: EmitService,
    private emitDirectService: EmitDirectService,
    private emitTopicService: EmitTopicService,
    private newTaskService: NewTaskService,
  ) {}

  getItems(code: string): Observable<ItemDto[]> {
    const path = '/item/getitem';
    return this.apiService.get(path).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }
  async fetchOracleQueryData() {
    let dbResult = await this.oracleDBervice.executeQuery(
      'rfdest',
      'SELECT * FROM TASKS WHERE tasktype=:tasktype AND id=:id',
      ['PI', '934FBA351852BFE8E053839CCA0A94D8'],
    );
    return dbResult;
  }
  async fetchOracleStoredProcData(taskEntity: TaskEntity) {
    let localConnection: any;
    let result: any;
    const numRows = 100;
    let dbResult: Array<any> = [];
    try {
      let rows;
      const localConnection = this.oracleDBervice.getOracleConnection('rfdest');
      await localConnection.then(async connection => (
          result = await connection.execute(
            `BEGIN
              phy_gettask_r_p(:in_facility, :in_custid, :in_location, :out_tasks);
           END;`,
            {
              in_facility: taskEntity.Facility, // Bind type is determined from the data.  Default direction is BIND_IN
              in_custid: taskEntity.CustID,
              in_location: taskEntity.Location,
              out_tasks: { dir: oracledb.BIND_OUT, type: oracledb.CURSOR },
            },
          )
      ));
      do {
        rows = await result.outBinds.out_tasks.getRows(numRows); // get numRows rows at a time
        dbResult.push(...rows);
      } while (rows.length === numRows);

      return dbResult;
    } finally {
      if (localConnection) {
        try {
          await localConnection.close();
        } catch (err) {
          console.log('Error when closing the database connection: ', err);
        }
      }
    }
  }

  async fetchPGStoredProcData() {
    let dbResult = await this.postgressDBService.executeQuery(
      'shipperanalyticsq',
      //'SELECT * FROM get_shipper_request_by_id(\'355026f6-1896-4c82-953a-375f44e45952\')',
      'select * from get_shipper_request_summary(current_date,0,10)',
      [],
    );
    return dbResult;
  }

  async fetchPGQueryData() {
    let dbResult = await this.postgressDBService.executeQuery(
      'shipperanalyticsq',
      'SELECT * FROM "TESTMASTER"',
      [],
    );
    return dbResult;
  }

  async publish(messageEntity: MessageEntity) {
    this.emitService.publish(messageEntity.Name, messageEntity.Message);
  }
  async publishDirect(messageEntity: MessageEntity) {
    this.emitDirectService.publish(messageEntity.Name, messageEntity.Message);
  }
  async publishTopic(messageEntity: MessageEntity) {
    this.emitTopicService.publish(messageEntity.Name, messageEntity.Message);
  }
  async newTask(messageEntity: MessageEntity) {
    this.newTaskService.publish(messageEntity.Name, messageEntity.Message);
  }
  addItem(itemDto: ItemDto): Observable<ItemDto[]> {
    const path = '/item/additem';
    return this.apiService.post(path, itemDto).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }

  updateItem(itemDto: ItemDto): Observable<ItemDto[]> {
    const path = '/item/updateitem';
    return this.apiService.put(path, itemDto).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }

  deleteItem(code: string): Observable<ItemDto[]> {
    const path = '/item/deleteitem';
    return this.apiService.delete(path, code).pipe(
      map(resp => {
        return resp.data;
      }),
    );
  }
}
