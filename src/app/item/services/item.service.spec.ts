import { Test, TestingModule } from '@nestjs/testing';
import { ItemService } from './item.service';
import { AppLoggerService, DependencyUtlilizationService, RxJSRetryOptions, DomainApiService, OracleDBService, PostgressDBService, EmitService, EmitDirectService, EmitTopicService, NewTaskService, ConfigService } from './../../helpers';
import { HttpService } from '@nestjs/common';
import { RxJSRetryService } from 'src/app/helpers/domain-api/services';




describe('ItemService', () => {
  let service: ItemService;
  const option = {
    filterOptions: { id: '1' },
    interceptorOptions: { id: '1' },
    loggerOptions: { appPath: process.cwd() },
    domainAPIOption: { host: 'http://localhost/', port: 3001 },
    oracleDBOptions: [
      {
        name: 'rfdest',
        user: 'rfdest',
        password: 'rfdest',
        connectString: 'alsyntstb.ohlogistics.com:1521/syntstb',
      },
      {
        name: 'DOCGENDEV',
        user: 'DOCGENDEV',
        password: 'DocGen$dev18',
        connectString: 'alsynwebd.ohlogistics.com/synwebd_maint',
      },
    ],
    postgressDBOptions: [
      {
        name: 'shipperanalyticsq',
        user: 'usr_shipper',
        host: 'FDC00pgsp305L.ohlogistics.com',
        database: 'shipperanalyticsq',
        password: 'Sh1pP3Rq@',
        port: 5432,
      },
    ],
    rabbitMQOptions: [        {
      name: 'synapsebffcore',
      key: 'info',
      severity: 'severity',
      exchangeName: 'DirectX',
      exchangeType: 'Direct',
      connection: 'amqp://guest:guest@localhost:5672/?prefetch-count=1',
    },]
  };
  const logger = new AppLoggerService( );
  let dependencyUtlilizationService: DependencyUtlilizationService = new DependencyUtlilizationService();


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ItemService,
        AppLoggerService,
        DomainApiService,
        HttpService,
        ConfigService,
        OracleDBService,
        PostgressDBService,
        EmitService,
        EmitDirectService,
        EmitTopicService,
        NewTaskService,
        DependencyUtlilizationService,
        RxJSRetryService,
      ],
    }).compile();

    service = module.get<ItemService>(ItemService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
