import { IsString } from 'class-validator';
export class MessageEntity {
    @IsString()
    Name:string;
    @IsString()
    Message: string;
}
