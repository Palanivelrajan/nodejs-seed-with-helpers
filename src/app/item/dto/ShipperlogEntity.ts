import { IsString, IsInt } from 'class-validator';
export class ShipperlogEntity {
    @IsString()
    logid: string;
    @IsString()
    logjson: string;
    @IsInt()
    createdjson: number;
}
