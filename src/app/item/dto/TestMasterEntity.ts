import { IsString, IsInt, IsDate } from 'class-validator';
export class TestMasterEntity {
    @IsString()
    ID
    @IsString()
    TRANSACTIO: string
    @IsString()
    NID: string
    @IsDate()
    TIMESSTAMP: Date
    @IsString()
    VALUE:string;
    @IsString()
    REQUESTID: string;
    @IsString()
    USERNAME: string;

}
