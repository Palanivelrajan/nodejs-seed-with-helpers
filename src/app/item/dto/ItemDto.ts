import { IsString, IsInt } from 'class-validator';
export class ItemDto {
    @IsString()
    Code: string;
    @IsString()
    Name: string;
    @IsInt()
    Price: number;
    @IsString()
    Category: string;
}
