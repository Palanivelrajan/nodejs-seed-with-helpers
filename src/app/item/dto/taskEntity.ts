import { IsString, IsInt, IsDate, IsOptional } from 'class-validator';
export class TaskEntity {
  @IsString()
  @IsOptional()
  ID: string;
  @IsString()
  @IsOptional()
  TaskType: string;
  @IsDate()
  @IsOptional()
  CreateDate: Date;
  @IsString()
  Facility: string;
  @IsString()
  CustID: string;
  @IsString()
  Location: string;
  @IsInt()
  @IsOptional()
  Priority: string;
}
