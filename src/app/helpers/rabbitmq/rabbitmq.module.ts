import { Module, DynamicModule } from '@nestjs/common';
import { EmitService } from './service/emit.service';
import { EmitDirectService, EmitTopicService, NewTaskService } from './service';
@Module({})
export class RabbitmqModule {
  static forRoot(): DynamicModule {
    return {
      module: RabbitmqModule,
      providers: [
        EmitService,
        EmitDirectService,
        EmitTopicService,
        NewTaskService,
      ],
      exports: [
        EmitService,
        EmitDirectService,
        EmitTopicService,
        NewTaskService,
      ],
    };
  }
}
