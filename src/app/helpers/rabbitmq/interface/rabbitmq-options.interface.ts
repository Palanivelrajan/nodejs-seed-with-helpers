export interface RabbitMQOptions {
  name: string;
  key: string;
  severity: string;
  exchangeName: string;
  exchangeType: string;
  connection: string;
}
