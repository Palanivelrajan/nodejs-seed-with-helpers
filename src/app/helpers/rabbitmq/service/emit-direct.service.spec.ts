import { Test, TestingModule } from '@nestjs/testing';
import { EmitDirectService } from './emit-direct.service';
import { RABBITMQ_OPTIONS } from '../constant';
import { ConfigModule } from '../../config';
//import * as amqp from 'amqplib';

describe('EmitLogDirectService', () => {
  let emitDirectService: EmitDirectService;
  let rabbitMQOptions = [
    {
      name: 'synapsebffcore',
      key: 'info',
      severity: 'severity',
      exchangeName: 'DirectX',
      exchangeType: 'Direct',
      connection: 'amqp://guest:guest@localhost:5672/?prefetch-count=1',
    },
  ];

  var QUEUE_OPTS = { durable: false };
  var EX_OPTS = { durable: false };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [
        EmitDirectService,
        {
          provide: RABBITMQ_OPTIONS,
          useValue: rabbitMQOptions,
        },
      ],
    }).compile();

    emitDirectService = module.get<EmitDirectService>(EmitDirectService);
  });

  it('should be defined', () => {
    expect(emitDirectService).toBeDefined();
  });
  // it('call publish with invalid queueName and check exception throw.', () => {
  //   try {
  //     emitDirectService.publish('', 'This is unit testing');
  //   } catch (ex) {
  //     expect(ex).not.toBeNull();
  //   }
  // });

  // it('call publish with queue Name and it should be called once', () => {
  //   jest.spyOn(emitDirectService, 'publish');
  //   emitDirectService.publish('synapsebffcore', 'This is unit testing');
  //   expect(emitDirectService.publish).toBeCalled();
  // });
});
