import { Test, TestingModule } from '@nestjs/testing';
import { EmitTopicService } from './emit-topic.service';
import { ConfigModule } from '../../config';

describe('EmitLogTopicService', () => {
  let emitTopicService: EmitTopicService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [EmitTopicService],
    }).compile();

    emitTopicService = module.get<EmitTopicService>(EmitTopicService);
  });

  it('should be defined', () => {
    expect(emitTopicService).toBeDefined();
  });

  // it('call publish with invalid queueName and check exception throw.', () => {
  //   try {
  //     emitTopicService.publish('', 'This is unit testing');
  //   } catch (ex) {
  //     expect(ex).not.toBeNull();
  //   }
  // });

  // it('call publish with queue Name and check it is called', () => {
  //   jest.spyOn(emitTopicService, 'publish');
  //   emitTopicService.publish('synapsebffcore', 'This is unit testing');
  //   expect(emitTopicService.publish).toBeCalled();
  // });
});
