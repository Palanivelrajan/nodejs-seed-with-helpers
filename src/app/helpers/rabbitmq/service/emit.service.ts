import { Injectable } from '@nestjs/common';
import * as amqp from 'amqplib';
import { ConfigService } from '../../config';
@Injectable()
export class EmitService {
  constructor(
    private readonly config: ConfigService,
  ) { }

  publish(queueName: string, message: string) {
    const rabbitMQOption = this.config.getRabbitmqOption(queueName);
    if (!rabbitMQOption) {
      throw Error('Connection string does not exist');
    } else {
      const conn = amqp
        .connect(rabbitMQOption.connection)
        .then((conn) => {
          return conn
            .createChannel()
            .then((ch) => {
              const ex = rabbitMQOption.exchangeName;
              const ok = ch.assertExchange(ex, rabbitMQOption.exchangeType, {
                durable: false,
              });
              return ok.then(() => {
                ch.publish(ex, '', Buffer.from(message));
                return ch.close();
              });
            })
            .finally(() => {
              conn.close();
            });
        })
        .catch(console.warn);
    }
  }
}
