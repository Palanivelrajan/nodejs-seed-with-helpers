export interface DomainAPIOptions {
  key: string;
  url: string;
}
