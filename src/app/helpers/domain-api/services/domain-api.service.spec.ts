import { HttpModule, HttpService } from '@nestjs/common';

import { Test, TestingModule } from '@nestjs/testing';
import { AxiosResponse } from 'axios';
import { DependencyUtlilizationService } from '../../healthcheck/dependency-utlilization/dependency-utlilization.service';
import { DOMAINAPI_MODULE_OPTIONS } from '../constants/domain-api.constants';
import { DomainApiService } from './domain-api.service';
import { AppLoggerService } from '../../logger';
import { Observable, of, throwError, observable } from 'rxjs';
import { ConfigService, ConfigModule } from '../../config';
import { RxJSRetryService } from './rxjs-retry.service';

describe('DomainApiService', () => {
  let domainApiService: DomainApiService;
  let dependencyUtlilizationService: DependencyUtlilizationService;
  let httpService: HttpService;
  let rxJSRetryService: RxJSRetryService;
  beforeEach(async () => {
    const domainAPIOption = {
      host: 'http://localhost',
      port: 3000,
    };
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [
        DomainApiService,
        AppLoggerService,
        RxJSRetryService,
        DependencyUtlilizationService,
      ],
    }).compile();

    domainApiService = module.get<DomainApiService>(DomainApiService);
    dependencyUtlilizationService = module.get<DependencyUtlilizationService>(
      DependencyUtlilizationService,
    );
    httpService = module.get<HttpService>(HttpService);
    rxJSRetryService = module.get<RxJSRetryService>(RxJSRetryService);
  });

  it('should be defined', () => {
    expect(domainApiService).toBeDefined();
  });
  it('Call onModuleInit and check the  ', () => {
    jest.spyOn(domainApiService, 'onModuleInit');
    domainApiService.onModuleInit();
    expect(domainApiService.baseURL).toBe('http://localhost:3001');
  });

  it('Get Method call throw error', () => {
    jest.fn(throwError);
    jest
      .spyOn(httpService, 'get')
      .mockReturnValue(throwError('divisible by six'));
    domainApiService.get('path');
    expect(httpService.get).toBeCalled();
    // expect(throwError).toBeCalled();
  });
  it('Get Method call throw error', done => {
    let counter = 0;
    rxJSRetryService.genericRetryStrategy = jest
      .fn()
      .mockReturnValueOnce(() => throwError(new Error('This should error')));
    jest
      .spyOn(httpService, 'get')
      .mockReturnValue(throwError(new Error('This should error')));

    domainApiService.get('path').subscribe({
      next: val => {
        expect(val).toBe(++counter);
      },
      error: err => {
        expect(err.message).toBe('This should error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
      complete: () => done(),
    });
  });
  it('Post Method call', () => {
    jest
      .spyOn(httpService, 'post')
      .mockReturnValue(new Observable<AxiosResponse>());
    domainApiService.post('path');
    expect(httpService.post).toBeCalled();
  });
  it('Post Method call throw error', done => {
    let counter = 0;
    rxJSRetryService.genericRetryStrategy = jest
      .fn()
      .mockReturnValueOnce(() => throwError(new Error('This should error')));
    jest
      .spyOn(httpService, 'post')
      .mockReturnValue(throwError(new Error('This should error')));

    domainApiService.post('path').subscribe({
      next: val => {
        expect(val).toBe(++counter);
      },
      error: err => {
        expect(err.message).toBe('This should error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
      complete: () => done(),
    });
  });

  it('Put Method call', () => {
    jest
      .spyOn(httpService, 'put')
      .mockReturnValue(new Observable<AxiosResponse>());
    domainApiService.put('path');
    expect(httpService.put).toBeCalled();
  });

  it('Put Method call throw error', done => {
    let counter = 0;
    rxJSRetryService.genericRetryStrategy = jest
      .fn()
      .mockReturnValueOnce(() => throwError(new Error('This should error')));
    jest
      .spyOn(httpService, 'put')
      .mockReturnValue(throwError(new Error('This should error')));

    domainApiService.put('path').subscribe({
      next: val => {
        expect(val).toBe(++counter);
      },
      error: err => {
        expect(err.message).toBe('This should error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
      complete: () => done(),
    });
  });

  it('Delete Method call', () => {
    jest
      .spyOn(httpService, 'delete')
      .mockReturnValue(new Observable<AxiosResponse>());
    domainApiService.delete('path');
    expect(httpService.delete).toBeCalled();
  });

  it('Delete Method call throw error', done => {
    let counter = 0;
    rxJSRetryService.genericRetryStrategy = jest
      .fn()
      .mockReturnValueOnce(() => throwError(new Error('This should error')));
    jest
      .spyOn(httpService, 'delete')
      .mockReturnValue(throwError(new Error('This should error')));

    domainApiService.delete('path').subscribe({
      next: val => {
        expect(val).toBe(++counter);
      },
      error: err => {
        expect(err.message).toBe('This should error');
        expect(rxJSRetryService.genericRetryStrategy).toBeCalledTimes(1);
        done();
      },
      complete: () => done(),
    });
  });
});
