import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { RxJSRetryService } from './rxjs-retry.service';
import { of, Observable } from 'rxjs';
import { retryWhen } from 'rxjs/operators';
import { Observer } from 'zen-observable-ts';
import { AppLoggerService } from '../../logger';

const createSource = (retryMax = 2): Observable<any> => {
  let retryCount = 0;
  return Observable.create((observer: Observer<any>) => {
    if (retryCount < retryMax) {
      retryCount++;
      const thrownError = new Error('Error');
      (thrownError as any).status = 400;
      observer.error(thrownError);
    } else {
      observer.next(retryCount);
      observer.complete();
    }
  });
};

describe('RxjsService', () => {
  let rxJSRetryService: RxJSRetryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RxJSRetryService, AppLoggerService],
    }).compile();

    rxJSRetryService = module.get<RxJSRetryService>(RxJSRetryService);
  });

  it('should be defined', () => {
    expect(rxJSRetryService).toBeDefined();
  });
  describe('genericRetryStrategy', () => {
    it('should retry and pass', done => {
      let finalVal = 0;
      const source = createSource();
      console.log('createSource ', finalVal)
      source
        .pipe(retryWhen(rxJSRetryService.genericRetryStrategy()))
        .subscribe({
          next: (val: any) => {
            finalVal = val;
            console.log('next', finalVal)
          },
          error: (err: Error) => {
            throw err;
          },
          complete: () => {
            console.log('finalVal', finalVal)
            expect(finalVal).toBe(30);
            done();
          },
        });
    });
    it('should retry and fail after max attempts', done => {
      const source = createSource(100);
      source
        .pipe(retryWhen(rxJSRetryService.genericRetryStrategy()))
        .subscribe({
          next: val => {
            throw new Error('Expected error, got ' + val);
          },
          error: err => {
            expect(err.message).toBe('Error');
            done();
          },
          complete: () => {
            throw new Error('Expected error but Observable completed');
          },
        });
    });
    it('should retry and fail from ignoredError option', done => {
      const source = createSource(100);
      source
        .pipe(
          retryWhen(
            rxJSRetryService.genericRetryStrategy({
              numberOfAttempts: 3,
              delayTime: 10,
              ignoredErrorCodes: [400],
            }),
          ),
        )
        .subscribe({
          next: val => {
            throw new Error('Expected error, got ' + val);
          },
          error: err => {
            expect(err.message).toBe('Error');
            done();
          },
          complete: () => {
            throw new Error('Expected error but Observable completed');
          },
        });
    });
  });
});
