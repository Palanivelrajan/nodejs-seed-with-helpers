import { PromInstanceCounter, PromMethodCounter } from '@digikare/nestjs-prom';
import { HttpService, Injectable, OnModuleInit } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { Observable, throwError as observableThrowError } from 'rxjs';
import { catchError, map, retryWhen } from 'rxjs/operators';
import { ConfigService } from '../../config/config.service';
import { DependencyUtlilizationService } from '../../healthcheck/dependency-utlilization/dependency-utlilization.service';
import { RxJSRetryService } from './rxjs-retry.service';

@PromInstanceCounter
@Injectable()
export class DomainApiService implements OnModuleInit {
  baseURL: string;
  constructor(
    private readonly config: ConfigService,
    private readonly httpService: HttpService,
    private readonly dependencyUtlilizationService: DependencyUtlilizationService,
    private readonly rxJSUtilsService: RxJSRetryService,
  ) {}
  onModuleInit() {
    this.baseURL = this.config.domainAPIOptions[0].url;
  }
  private formatErrors(error: any) {
    return observableThrowError(error);
  }

  private updateLastUtilizedTimestamp() {
    this.dependencyUtlilizationService.updateTimeStamp('domain_api');
  }
  @PromMethodCounter()
  post(path: string, body: any = {}): Observable<AxiosResponse> {
    this.updateLastUtilizedTimestamp();
    return this.httpService.post(this.baseURL + path, body).pipe(
      retryWhen(this.rxJSUtilsService.genericRetryStrategy()),
      catchError(this.formatErrors),
    );
  }
  @PromMethodCounter()
  get(path: string, body: any = {}): Observable<AxiosResponse<any>> {
    const url = this.baseURL + path;
    this.updateLastUtilizedTimestamp();
    return this.httpService.get(url, { validateStatus: null }).pipe(
      retryWhen(
        this.rxJSUtilsService.genericRetryStrategy({
          numberOfAttempts: 2,
          delayTime: 2000,
          ignoredErrorCodes: [500],
        }),
      ),
      catchError(this.formatErrors),
    );
  }
  @PromMethodCounter()
  put(path: string, body: any = {}): Observable<AxiosResponse> {
    this.updateLastUtilizedTimestamp();
    return this.httpService.put(this.baseURL + path, body).pipe(
      retryWhen(
        this.rxJSUtilsService.genericRetryStrategy({
          numberOfAttempts: 2,
          delayTime: 2000,
          ignoredErrorCodes: [500],
        }),
      ),

      catchError(this.formatErrors),
    );
  }
  @PromMethodCounter()
  delete(path: string, body: any = {}): Observable<AxiosResponse> {
    this.updateLastUtilizedTimestamp();
    return this.httpService.delete(this.baseURL + path).pipe(
      retryWhen(
        this.rxJSUtilsService.genericRetryStrategy({
          numberOfAttempts: 2,
          delayTime: 2000,
          ignoredErrorCodes: [500],
        }),
      ),

      catchError(this.formatErrors),
    );
  }
}
