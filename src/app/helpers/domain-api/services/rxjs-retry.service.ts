import { Injectable } from '@nestjs/common';
import { GenericRetryOptions } from '../interfaces/generic-retry-options.interface';
import { Observable, throwError, timer } from 'rxjs';
import { mergeMap, finalize } from 'rxjs/operators';
import { PromInstanceCounter } from '@digikare/nestjs-prom';
import { AppLoggerService } from '../../logger';
import { RxJSRetryOptions } from '../interfaces/rxjs-retry-options.interface';
import { defaultRetryOptions } from '../constants/rxjs-retry-options.constants';
@PromInstanceCounter
@Injectable()
export class RxJSRetryService {
  constructor(private readonly logger: AppLoggerService) {}

  genericRetryStrategy<T extends any>(
    options?: RxJSRetryOptions,
  ): (obs: Observable<T>) => Observable<number> {
    options = { ...defaultRetryOptions, ...options };
    return obs =>
      obs.pipe(
        mergeMap((error, i) => {
          i++;
          if (
            i > options.numberOfAttempts ||
            options.ignoredErrorCodes.find(e => e === error.status)
          ) {
            return throwError(error);
          }
          return timer(options.delayTime * i);
        }),
        // finalize(() =>
        //   this.logger.log(
        //     'Finished' +
        //       RxJRetryService.name +
        //       ' ' +
        //       this.genericRetryStrategy.name,
        //   ),
        // ),
      );
  }
}
