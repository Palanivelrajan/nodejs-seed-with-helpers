import { DynamicModule, Global, HttpModule, Module } from '@nestjs/common';
import { ConfigModule } from '../config/config.module';
import { HealthCheckModule } from '../healthcheck/health-check.module';
import { DomainApiService } from './services/domain-api.service';
import { RxJSRetryService } from './services/rxjs-retry.service';

@Global()
@Module({})
export class DomainApiModule {
  static forRoot(): DynamicModule {
    return {
      imports: [HttpModule, HealthCheckModule, ConfigModule],
      module: DomainApiModule,
      providers: [DomainApiService, RxJSRetryService],
      exports: [DomainApiService, RxJSRetryService],
    };
  }
}
