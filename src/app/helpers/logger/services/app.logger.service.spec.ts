import { Test, TestingModule } from '@nestjs/testing';
import { AppLoggerService } from './app.logger.service';
import { LogMessage } from './../interfaces/logmessage.interface';
import { LOGGER_MODULE_OPTIONS } from '../constants/logger.constants';
import { createLogger, format, Logger, transports } from 'winston';

describe('LoggerService', () => {
  let appLoggerService: AppLoggerService;
  const message: LogMessage = {
    Title: 'This is unit testing',
    Type: 'Unit Test',
    Detail: 'Unit Test - This is detail of the log',
    Status: 'Open',
  };

  beforeAll(async () => {
    const loggerOptions = { appPath: process.cwd() };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AppLoggerService,
        {
          provide: LOGGER_MODULE_OPTIONS,
          useValue: loggerOptions,
        },
      ],
    }).compile();

    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
    appLoggerService.logger = createLogger({
      level: 'info',
      format: format.json(),
      transports: [new transports.Console({ level: 'info', silent: true })],
    });
  });

  it('should be defined', () => {
    expect(appLoggerService).toBeDefined();
  });

  it('log should be called', () => {
    message.Title = 'Log';
    const spy = jest.spyOn(appLoggerService.logger, 'log');
    appLoggerService.log(message);
    expect(spy).toHaveBeenCalledWith('info', 'info : ', message);
  });
  it('error should be called', () => {
    message.Title = 'error';
    appLoggerService.error(message, '');
    const spy = jest.spyOn(appLoggerService.logger, 'log');
    appLoggerService.debug(message);
    expect(spy).toHaveBeenCalledWith('error', 'error : ', message);
  });

  it('debug should be called', () => {
    message.Title = 'debug';
    const spy = jest.spyOn(appLoggerService.logger, 'log');
    appLoggerService.debug(message);
    expect(spy).toHaveBeenCalledWith('debug', 'debug : ', message);
  });
  it('verbose should be called', () => {
    message.Title = 'verbose';
    const spy = jest.spyOn(appLoggerService.logger, 'log');
    appLoggerService.verbose(message);
    expect(spy).toHaveBeenCalledWith('verbose', 'verbose : ', message);
  });
  it('warn should be called', () => {
    message.Title = 'warn';
    const spy = jest.spyOn(appLoggerService.logger, 'log');
    appLoggerService.warn(message);
    expect(spy).toHaveBeenCalledWith('warn', 'warn : ', message);
  });
  it('get logger', () => {
    message.Title = 'warn';
    const output = appLoggerService.getlogger();
    expect(output).toEqual(appLoggerService.logger.transports.length);
  });

  it('IsFileLog eq false', async () => {
    appLoggerService.IsFileLog = false;
    appLoggerService.onModuleInit();
    await new Promise(r => setTimeout(r, 2000));
    expect(appLoggerService.logger.transports.length).toEqual(0);
  });
  it('IsFileLog eq true', () => {
    appLoggerService.IsFileLog = true;
    appLoggerService.onModuleInit();
    expect(appLoggerService.logger.transports.length).toEqual(4);
  });
});
