import { Test, TestingModule } from '@nestjs/testing';
import { PostgressDBService } from './postgress-db.service';
import { POSTGRESS_DB_OPTIONS } from '../constant';
import { Pool } from 'pg';
import { ConfigModule, ConfigService } from '../../../config';
import { PositionalOptions } from 'yargs';
describe('PostgressDBService', () => {
  let postgressDBService: PostgressDBService;
  let postgressDBOptions = [
    {
      name: 'shipperanalyticsq',
      user: 'usr_shipper',
      host: 'FDC00pgsp305L.ohlogistics.com',
      database: 'shipperanalyticsq',
      password: 'Sh1pP3Rq@',
      port: 5432,
    },
  ];
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [PostgressDBService],
    }).compile();

    postgressDBService = module.get<PostgressDBService>(PostgressDBService);
    //let pool = module.get<Pool>(Pool);
  });

  it('should be defined', () => {
    expect(postgressDBService).toBeDefined();
  });

  it('ExecuteQuery should be success', () => {
    expect(
      postgressDBService
        .executeQuery(
          postgressDBOptions[0].name,
          'select * from get_shipper_request_summary(current_date,0,10)',
          [],
        )
        .then(resolve => {
          expect(resolve).not.toBeNull();
        }),
    );
  });

  it('ExecuteQuery throw error', () => {
    expect(
      postgressDBService
        .executeQuery(
          '',
          'select * from get_shipper_request_summary(current_date,0,10)',
        )
        .then(resolve => {
          expect(resolve).not.toBeNull();
        })
        .catch(rejected => {
          expect(rejected).not.toBeNull();
        }),
    );
  });
});
