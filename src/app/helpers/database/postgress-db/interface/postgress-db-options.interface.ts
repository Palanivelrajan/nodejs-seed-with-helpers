export interface PostgressDBOptions {
  name: string;
  user: string;
  host: string;
  database: string;
  password: string;
  port: number;
}
