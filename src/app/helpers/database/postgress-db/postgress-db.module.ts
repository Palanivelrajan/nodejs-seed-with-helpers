import { Module, Global, DynamicModule } from '@nestjs/common';
import { PostgressDBService } from './service';
@Global()
@Module({
  providers: [PostgressDBService],
  exports: [PostgressDBService],
})
export class PostgressDBModule {
}
