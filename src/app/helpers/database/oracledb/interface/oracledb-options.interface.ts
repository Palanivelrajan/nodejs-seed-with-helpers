export interface OracleDBOptions {
  name: string;
  user: string;
  password: string;
  connectString: string;
}
