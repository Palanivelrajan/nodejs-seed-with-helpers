import { Module, DynamicModule, Global } from '@nestjs/common';
import { OracleDBService } from './service/oracledb.service';

@Global()
@Module({
  providers: [OracleDBService],
  exports: [OracleDBService],
})
export class OracleDBModule {
}
