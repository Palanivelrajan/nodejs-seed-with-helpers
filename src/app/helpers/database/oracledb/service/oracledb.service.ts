import { Injectable } from '@nestjs/common';
import { getConnection } from 'oracledb';
import { PromInstanceCounter, PromMethodCounter } from '@digikare/nestjs-prom';
import { ConfigService } from '../../../config';
import { timer } from 'rxjs';

@PromInstanceCounter
@Injectable()
export class OracleDBService {
  constructor(private readonly config: ConfigService) {}
  @PromMethodCounter()
  async executeQuery(
    dbName: string,
    query: string,
    bindValue: any[] = [],
  ): Promise<any> {
    try {
      const conn = await this.getOracleConnection(dbName);
      console.log(conn);
      return conn.execute(query, bindValue);
    } catch (err) {
      throw err;
    }
  }

  @PromMethodCounter()
  async getOracleConnection(dbName: string) {
    const oracleDbOption = this.config.getOracelDatabaseOption(dbName);
    if (!oracleDbOption) {
      throw new Error('Connection string does not exist');
    } else {
      return getConnection({
        user: oracleDbOption.user,
        password: oracleDbOption.password,
        connectString: oracleDbOption.connectString,
      });
    }
  }
}
