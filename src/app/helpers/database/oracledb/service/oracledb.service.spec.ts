import { Test, TestingModule } from '@nestjs/testing';
import { OracleDBService } from './oracledb.service';
import { ORACLE_DB_OPTIONS } from '../constant';
import { OracleDBOptions } from '../interface';
import * as oracledb from 'oracledb';
import { ConfigModule } from '../../../config';

describe('OracleDbService', () => {
  let oracleDBService: OracleDBService;

  let oracleDBOptions = [
    {
      name: 'rfdest',
      user: 'rfdest',
      password: 'rfdest',
      connectString: 'alsyntstb.ohlogistics.com:1521/syntstb',
    },
    {
      name: 'DOCGENDEV',
      user: 'DOCGENDEV',
      password: 'DocGen$dev18',
      connectString: 'alsynwebd.ohlogistics.com/synwebd_maint',
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [
        OracleDBService,
        {
          provide: ORACLE_DB_OPTIONS,
          useValue: oracleDBOptions,
        },
      ],
    }).compile();

    oracleDBService = module.get<OracleDBService>(OracleDBService);
  });

  it('should be defined', () => {
    expect(oracleDBService).toBeDefined();
  });

  // Only changed this test function, the others should be able to follow suit
  it('OracleDBService executeQuery success', () => {
    // no need for jest.mock('oracledb'), your setting your mocks on your own
    // jest.mock('oracledb');
    // no need for this to be a function, it is a value, set the variable as such
    const executeMockValue = { id: 1, name: 'PAL' };
    // overall, this should really be mocking from the oracledb instead of your internal wrapper function
    // this way if you are needing to run the oracleDBService.getOracleConnection() function
    // you won't be dealing with a mock. In general, mocking a function in the class you are testing,
    // is kind of a really bad practice
    /* jest.spyOn(oracleDBService, 'getOracleConnection').mockResolvedValue({
      // no need to make this a mock function, the parent function is already mocked
      // you can change this as needed without making it a jest.fn()
      execute: () => executeMockValue,
    }); */
    oracledb.getConnection = jest.fn().mockResolvedValueOnce({
      execute: () => executeMockValue,
    });
    // use the jest syntax for promises for cleaner and easier to read tests
    expect(oracleDBService.executeQuery('rfdest', '')).resolves.toEqual(
      executeMockValue,
    );
  });

  it('OracleDBService getConnection success', () => {
    jest.mock('oracledb');

    let getConnMockValue = {
      execute: async function(query, bindValue) {},
      close: function() {},
    };

    // oracledb.getConnection = jest.fn().mockReturnValue(getConnMockValue);
    jest.fn(oracledb.getConnection).mockReturnValue(getConnMockValue);

    expect(oracleDBService.getOracleConnection('rfdest')).resolves.toEqual(getConnMockValue);
  });

  it('OracleDBService getConnection throw error', () => {
    oracleDBService.getOracleConnection('InvalidDB').catch(reject => {
      expect(reject).toEqual(new Error('Connection string does not exist'));
    });
  });
});
