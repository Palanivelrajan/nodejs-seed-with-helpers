import { DynamicModule, Module } from '@nestjs/common';
import { PromModule} from '@digikare/nestjs-prom';
import { MetricsController } from './controller/metrics.controller';
@Module({})
export class MetricsModule {
  static forRoot(): DynamicModule {
    return {
      imports: [
        PromModule.forRoot({
          withDefaultController: false,
        }),
      ],
      module: MetricsModule,
      providers: [],
      controllers: [MetricsController],
    };
  }
}
