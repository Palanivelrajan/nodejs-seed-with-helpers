import { Controller, Get, Res } from '@nestjs/common';
import * as client from 'prom-client';
import { PATH_METADATA } from '@nestjs/common/constants';

@Controller()
export class MetricsController {
  @Get('metrics')
  index() {
    // res.set('Content-Type', client.register.contentType);
    // res.end(client.register.metrics());
    return client.register.metrics();
  }

  public static forRoot(path: string) {
    Reflect.defineMetadata(PATH_METADATA, path, MetricsController);
    return MetricsController;
  }
}
