import { Test, TestingModule } from '@nestjs/testing';
import { DependencyUtlilizationService } from './dependency-utlilization.service';

describe('DependencyUtlilizationService', () => {
  let service: DependencyUtlilizationService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DependencyUtlilizationService],
    }).compile();

    service = module.get<DependencyUtlilizationService>(
      DependencyUtlilizationService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('get counter is defined', () => {
    expect(service.getLastUsedTimeStamp).toBeDefined();
  });

  it('set counter is defined', () => {
    expect(service.updateTimeStamp).toBeDefined();
  });

  it('set and get timestamp to the `api` key', () => {
    service.updateTimeStamp('api');
    expect(service.getLastUsedTimeStamp('api')).toBeDefined();
  });

  it('get timestamp before set to be undefined', () => {
    expect(service.getLastUsedTimeStamp('api')).toBeUndefined();
  });

  it('is Recently Used', () => {
    service.updateTimeStamp('oracle_test_db');
    expect(service.isRecentlyUsed('oracle_test_db')).toBeTruthy();
  });

  it('update TimeStamp', () => {
    service.updateTimeStamp('oracle_test_db');
    expect(service.isRecentlyUsed('oracle_test_db')).toBeTruthy();
  });

  it('is Not Recently Used > 5000', async () => {
    jest
      .spyOn(service, 'currentTimeStamp')
      .mockReturnValue(new Date().getSeconds() - 6000);
    service.updateTimeStamp('oracle_test_db');
    jest
      .spyOn(service, 'currentTimeStamp')
      .mockReturnValue(new Date().getSeconds());
    expect(service.isRecentlyUsed('oracle_test_db')).toBeFalsy();
  });

  it('is Recently Used < 5000', async () => {
    // jest.setTimeout(30000);
    jest
      .spyOn(service, 'currentTimeStamp')
      .mockReturnValue(new Date().getSeconds() - 4000);
    service.updateTimeStamp('oracle_test_db');
    jest
      .spyOn(service, 'currentTimeStamp')
      .mockReturnValue(new Date().getSeconds());
    expect(service.isRecentlyUsed('oracle_test_db')).toBeTruthy();
  });

  it('initial service call', async () => {
    expect(service.isRecentlyUsed('oracle_test_db1')).toBeFalsy();
  });
});
