import { DynamicModule, Module } from '@nestjs/common';
import { ApiHealthCheckModule } from './api-health-check/api-health-check.module';
import { DependencyUtlilizationService } from './dependency-utlilization/dependency-utlilization.service';
import { HealthCheckController } from './health-check.controller';
import { HealthCheckService } from './health-check.service';
import { PostgressdbHealthCheckModule } from './postgressdb-health-check/postgressdb-health-check.module';
import { OracledbHealthCheckModule } from './oracledb-health-check/oracledb-health-check.module';
import { AppLoggerService } from '../logger';
@Module({})
export class HealthCheckModule {
  static forRoot(): DynamicModule {
    return {
      module: HealthCheckModule,
      imports: [
        ApiHealthCheckModule,
        PostgressdbHealthCheckModule,
        OracledbHealthCheckModule,
        AppLoggerService,
      ],
      providers: [HealthCheckService, DependencyUtlilizationService],
      controllers: [HealthCheckController],
      exports: [DependencyUtlilizationService],
    };
  }
}
