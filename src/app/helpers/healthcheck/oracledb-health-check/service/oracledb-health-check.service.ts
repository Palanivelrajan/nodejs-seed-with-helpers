import { Injectable } from '@nestjs/common';
import { ComponentType } from './../../enums/component-type.enum';
import { HealthStatus } from './../../enums/health-status.enum';
import { OracleDBService, OracleDBOptions } from '../../../database/oracledb';
import { ComponentHealthCheckResult } from '../../interfaces/component-health-check-result.interface';
import { DependencyUtlilizationService } from '../../dependency-utlilization/dependency-utlilization.service';
import { OracleDBHealthCheckOptions } from '../interface/oracledb-health-check-options.interface';
import { rejects } from 'assert';
import { AppLoggerService } from '../../../logger';
@Injectable()
export class OracledbHealthCheckService {
  private healthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  };
  constructor(
    private readonly oracleDBService: OracleDBService,
    private readonly dependencyUtilizationService: DependencyUtlilizationService,
    private logger: AppLoggerService,
  ) {
    this.healthIndicatorResponse = {};
  }

  private async pingCheck(oracleDB: OracleDBHealthCheckOptions) {
    return (
      this.dependencyUtilizationService.isRecentlyUsed(oracleDB.name) ||
      (await this.oracleDBService.executeQuery(
        'rfdest',
        'SELECT * FROM TASKS WHERE tasktype=:tasktype AND id=:id',
        ['PI', '934FBA351852BFE8E053839CCA0A94D8'],
      ))
    );
  }

  async isHealthy(
    listOfOracleDBList: OracleDBHealthCheckOptions[],
  ): Promise<{ [key: string]: ComponentHealthCheckResult }> {
    for (const oracleDB of listOfOracleDBList) {
      const apiHealthStatus = {
        status: HealthStatus.fail,
        type: ComponentType.database,
        componentId: oracleDB.name,
        description: `Health Status of ${oracleDB.name} is: fail`,
        time: Date.now(),
        output: '',
        links: {},
      };

      await this.pingCheck(oracleDB)
        .then(resolve => {
          apiHealthStatus.status = HealthStatus.pass;
          apiHealthStatus.description = `Health Status of ${oracleDB.name} is: pass`;
          this.healthIndicatorResponse[oracleDB.name] = apiHealthStatus;
        })
        .catch(reject => {
          const message = {
            Title: 'Postgress Health Check ',
            Type: 'Exception - Postgress Connection/Executing Query ',
            Detail: reject,
            Status: 'Failed',
          };

          this.logger.error(message, '');

          apiHealthStatus.output = reject;
          this.healthIndicatorResponse[oracleDB.name] = apiHealthStatus;
        });
    }
    return this.healthIndicatorResponse;
  }
}
