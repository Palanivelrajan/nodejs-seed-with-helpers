export interface PostgressDBHealthCheckOptions {
  name: string;
  user: string;
  host: string;
  database: string;
  password: string;
  port: number;
}
