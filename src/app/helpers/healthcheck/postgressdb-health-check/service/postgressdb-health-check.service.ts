import { Injectable } from '@nestjs/common';
import { PostgressDBHealthCheckOptions } from '../interface/postgressdb-health-check-options.interface';
import { ComponentType } from './../../enums/component-type.enum';
import { HealthStatus } from './../../enums/health-status.enum';
import { PostgressDBService } from '../../../database/postgress-db';
import { DependencyUtlilizationService } from '../../dependency-utlilization/dependency-utlilization.service';
import { ComponentHealthCheckResult } from '../../interfaces/component-health-check-result.interface';
import { AppLoggerService } from '../../../logger';
@Injectable()
export class PostgressdbHealthCheckService {
  private healthIndicatorResponse: {};
  constructor(
    private readonly postgressDBService: PostgressDBService,
    private readonly dependencyUtilizationService: DependencyUtlilizationService,
    private logger: AppLoggerService,
  ) {
    this.healthIndicatorResponse = {};
  }

  private async pingCheck(postgressDB: PostgressDBHealthCheckOptions) {
    return (
      this.dependencyUtilizationService.isRecentlyUsed(postgressDB.name) ||
      (await this.postgressDBService.executeQuery(
        'shipperanalyticsq',
        'SELECT * FROM "TESTMASTER"',
        [],
      ))
    );
  }

  async isHealthy(
    listOfPostgressList: PostgressDBHealthCheckOptions[],
  ): Promise<{ [key: string]: ComponentHealthCheckResult }> {
    for (const postgressDB of listOfPostgressList) {
      const apiHealthStatus = {
        status: HealthStatus.fail,
        type: ComponentType.database,
        componentId: postgressDB.name,
        description: `Health Status of ${postgressDB.database} is: fail`,
        time: Date.now(),
        output: '',
        links: {},
      };
      const result = await this.pingCheck(postgressDB)
        .then(resolve => {
          apiHealthStatus.status = HealthStatus.pass;
          apiHealthStatus.description = `Health Status of ${postgressDB.name} is: pass`;
          this.healthIndicatorResponse[postgressDB.name] = apiHealthStatus;
        })
        .catch(reject => {
          const message = {
            Title: 'Postgress Health Check ',
            Type: 'Exception - Postgress Connection/Executing Query ',
            Detail: reject,
            Status: 'Failed',
          };
          this.logger.error(message, '');
          apiHealthStatus.output = reject;
          this.healthIndicatorResponse[postgressDB.name] = apiHealthStatus;
        });
    }

    return this.healthIndicatorResponse;
  }
}
