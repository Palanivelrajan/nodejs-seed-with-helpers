import { Test, TestingModule } from '@nestjs/testing';
import { PostgressdbHealthCheckService } from './postgressdb-health-check.service';
import { DependencyUtlilizationService } from '../../dependency-utlilization/dependency-utlilization.service';
import { PostgressDBService } from './../../../database/postgress-db/service/postgress-db.service';
import { throwError, Observable, of } from 'rxjs';
import { async } from 'rxjs/internal/scheduler/async';
import { HealthStatus } from '../../enums/health-status.enum';
import { ConfigModule } from '../../../config';
import { AppLoggerService } from '../../../logger';

describe('PostgressdbHealthCheckService', () => {
  let postgressdbHealthCheckService: PostgressdbHealthCheckService;
  let postgressDBService: PostgressDBService;
  let dependencyUtlilizationService: DependencyUtlilizationService;
  let appLoggerService: AppLoggerService;
  let postgressdbOptions = [
    {
      name: 'shipperanalyticsq',
      user: 'usr_shipper',
      host: 'FDC00pgsp305L.ohlogistics.com',
      database: 'shipperanalyticsq',
      password: 'Sh1pP3Rq@',
      port: 5432,
    },
  ];

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [TestingModule, ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [
        PostgressdbHealthCheckService,
        DependencyUtlilizationService,
        PostgressDBService,
        AppLoggerService,
      ],
    }).compile();

    postgressdbHealthCheckService = module.get<PostgressdbHealthCheckService>(
      PostgressdbHealthCheckService,
    );

    postgressDBService = module.get<PostgressDBService>(PostgressDBService);
    dependencyUtlilizationService = module.get<DependencyUtlilizationService>(
      DependencyUtlilizationService,
    );

    appLoggerService = module.get<AppLoggerService>(AppLoggerService);
  });

  it('should be defined', () => {
    expect(postgressdbHealthCheckService).toBeDefined();
  });

  it('isHealthy should return status as true when pingCheck return true', () => {
    jest
      .spyOn(dependencyUtlilizationService, 'isRecentlyUsed')
      .mockReturnValue(true);

    jest
      .spyOn(postgressDBService, 'executeQuery')
      .mockImplementation(async (dbname, query, bindValue) => {
        {
        }
      });

    const result = postgressdbHealthCheckService.isHealthy(postgressdbOptions);

    result.then(response =>
      expect(response['shipperanalyticsq'].status).toBe(HealthStatus.pass),
    );
  });
  it('isHealthy should return status as false when pingCheck return false', () => {
    jest
      .spyOn(dependencyUtlilizationService, 'isRecentlyUsed')
      .mockReturnValue(false);

    jest
      .spyOn(postgressDBService, 'executeQuery')
      .mockImplementation(async (dbname, query, bindValue) => {
        throw new Error('This should error');
      });

    jest.spyOn(appLoggerService, 'error').mockImplementation(() => {});

    const result = postgressdbHealthCheckService.isHealthy(postgressdbOptions);

    result.catch(reject => {
      expect(reject['shipperanalyticsq'].status).toBe(HealthStatus.fail);
      expect(appLoggerService.error).toBeCalledTimes(1);
    });
  });
});
