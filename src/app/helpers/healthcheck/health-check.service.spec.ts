import { HttpService, HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ApiHealthIndicator } from './api-health-check/api-health-indicator';
import { HEALTH_CHECK_MODULE_OPTIONS } from './constants/health-check.contants';
import { DependencyUtlilizationService } from './dependency-utlilization/dependency-utlilization.service';
import { HealthCheckService } from './health-check.service';
import { PostgressdbHealthCheckService } from './postgressdb-health-check/service/postgressDB-health-check.service';
import { PostgressDBService } from '../database/postgress-db/service/postgress-db.service';
import { OracledbHealthCheckService } from './oracledb-health-check/service/oracledb-health-check.service';
import { OracleDBService } from '../database/oracledb';
import { HealthStatus } from './enums/health-status.enum';
import { ComponentType } from './enums/component-type.enum';
import { ConfigModule, ConfigService } from '../config';

import { HealthCheckOptions } from './interfaces/health-check-options.interface';
import { ComponentHealthCheckResult } from './interfaces/component-health-check-result.interface';
import { AppLoggerService } from '../logger';
describe('HealthCheckService', () => {
  let healthCheckService: HealthCheckService;
  let apiHealthIndicator: ApiHealthIndicator;
  let postgressdbHealthCheckService: PostgressdbHealthCheckService;
  let oracledbHealthCheckService: OracledbHealthCheckService;

  const healthCheckModuleOptions = {
    id: '1',
    dnsList: [{ key: 'domain_api', url: 'http://localhost:3001' }],
    databaseList: {
      oracleDBOptions: [
        {
          name: 'rfdest',
          user: 'rfdest',
          password: 'rfdest',
          connectString: 'alsyntstb.ohlogistics.com:1521/syntstb',
        },
      ],
      postgressDBOption: [
        {
          name: 'shipperanalyticsq',
          user: 'usr_shipper',
          host: 'FDC00pgsp305L.ohlogistics.com',
          database: 'shipperanalyticsq',
          password: 'Sh1pP3Rq@',
          port: 5432,
        },
      ],
    },
  };

  // const apiHealthStatus = {
  //   status: HealthStatus.fail,
  //   type: ComponentType.url,
  //   componentId: 'domain_api',
  //   description: `Health Status of is: fail`,
  //   time: Date.now(),
  //   output: '',
  //   links: {},
  // };
  let healthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  } = {
    domain_api: {
      status: HealthStatus.fail,
      type: ComponentType.url,
      componentId: 'domain_api',
      description: `Health Status of http://localhost:3001 is: fail`,
      time: Date.now(),
      output: '',
      links: {},
    },
  };

  let postgressHealthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  } = {
    shipperanalyticsq: {
      status: HealthStatus.fail,
      type: ComponentType.database,
      componentId: 'shipperanalyticsq',
      description: `Health Status of is: fail`,
      time: Date.now(),
      output: '',
      links: {},
    },
  };

  let oracleHealthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  } = {
    rfdest: {
      status: HealthStatus.fail,
      type: ComponentType.database,
      componentId: '',
      description: `Health Status of  is: fail`,
      time: Date.now(),
      output: '',
      links: {},
    },
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, ConfigModule.forRoot({ folder: '/src/config' })],
      providers: [
        HealthCheckService,
        PostgressdbHealthCheckService,
        ApiHealthIndicator,
        OracledbHealthCheckService,
        ApiHealthIndicator,
        DependencyUtlilizationService,
        PostgressdbHealthCheckService,
        PostgressDBService,
        DependencyUtlilizationService,
        OracledbHealthCheckService,
        OracleDBService,
        DependencyUtlilizationService,
        AppLoggerService,
      ],
    }).compile();

    healthCheckService = module.get<HealthCheckService>(HealthCheckService);
    apiHealthIndicator = module.get<ApiHealthIndicator>(ApiHealthIndicator);
    postgressdbHealthCheckService = module.get<PostgressdbHealthCheckService>(
      PostgressdbHealthCheckService,
    );
    oracledbHealthCheckService = module.get<OracledbHealthCheckService>(
      OracledbHealthCheckService,
    );
  });

  it('should be defined', () => {
    expect(healthCheckService).toBeDefined();
  });

  it('isHealthy fail', () => {
    jest
      .spyOn(apiHealthIndicator, 'isHealthy')
      .mockReturnValue(Promise.resolve(healthIndicatorResponse));

    jest
      .spyOn(postgressdbHealthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(postgressHealthIndicatorResponse));

    jest
      .spyOn(oracledbHealthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(oracleHealthIndicatorResponse));

    const result = healthCheckService.isHealthy();

    let healthCheckResult = {
      status: HealthStatus.fail,
      version: 'v1.0.0',
      releaseID: 'release1',
      notes: [' '],
      output: ' ',
      serviceID: '123123',
      description: `Health status of the BFF application is ${HealthStatus.fail}.`,
      details: [
        healthIndicatorResponse,
        oracleHealthIndicatorResponse,
        postgressHealthIndicatorResponse,
      ],
      links: {},
    };

    result.then(response => {
      //console.log('isHealthy pass', response);
      expect(response).toEqual(healthCheckResult);
    });
  });

  it('isHealthy pass', () => {
    healthIndicatorResponse['domain_api'].status = HealthStatus.warn;
    oracleHealthIndicatorResponse['rfdest'].status = HealthStatus.warn;
    postgressHealthIndicatorResponse['shipperanalyticsq'].status =
      HealthStatus.warn;

    jest
      .spyOn(apiHealthIndicator, 'isHealthy')
      .mockReturnValue(Promise.resolve(healthIndicatorResponse));

    jest
      .spyOn(postgressdbHealthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(postgressHealthIndicatorResponse));

    jest
      .spyOn(oracledbHealthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(oracleHealthIndicatorResponse));

    const result = healthCheckService.isHealthy();

    let healthCheckResult = {
      status: HealthStatus.warn,
      version: 'v1.0.0',
      releaseID: 'release1',
      notes: [' '],
      output: ' ',
      serviceID: '123123',
      description: `Health status of the BFF application is ${HealthStatus.warn}.`,
      details: [
        healthIndicatorResponse,
        oracleHealthIndicatorResponse,
        postgressHealthIndicatorResponse,
      ],
      links: {},
    };

    result.then(response => {
      //console.log('isHealthy pass', response);
      expect(response).toEqual(healthCheckResult);
    });
  });

  it('isHealthy pass', () => {
    healthIndicatorResponse['domain_api'].status = HealthStatus.pass;
    oracleHealthIndicatorResponse['rfdest'].status = HealthStatus.pass;
    postgressHealthIndicatorResponse['shipperanalyticsq'].status =
      HealthStatus.pass;

    jest
      .spyOn(apiHealthIndicator, 'isHealthy')
      .mockReturnValue(Promise.resolve(healthIndicatorResponse));

    jest
      .spyOn(postgressdbHealthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(postgressHealthIndicatorResponse));

    jest
      .spyOn(oracledbHealthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(oracleHealthIndicatorResponse));

    const result = healthCheckService.isHealthy();

    let healthCheckResult = {
      status: HealthStatus.pass,
      version: 'v1.0.0',
      releaseID: 'release1',
      notes: [' '],
      output: ' ',
      serviceID: '123123',
      description: `Health status of the BFF application is ${HealthStatus.pass}.`,
      details: [
        healthIndicatorResponse,
        oracleHealthIndicatorResponse,
        postgressHealthIndicatorResponse,
      ],
      links: {},
    };

    result.then(response => {
      //console.log('isHealthy pass', response);
      expect(response).toEqual(healthCheckResult);
    });
  });
});
