import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { DependencyUtlilizationService } from '../dependency-utlilization/dependency-utlilization.service';
import { ApiHealthIndicator } from './api-health-indicator';
import { ApiHealthCheckOptions } from './interfaces/api-health-check-options.interface';
import { HealthStatus } from '../enums/health-status.enum';
import { ComponentType } from '../enums/component-type.enum';
import { DomainAPIOptions } from '../../domain-api';

describe('ApiHealthIndicator', () => {
  let apiHealthIndicator: ApiHealthIndicator;
  let httpService: HttpService;
  let dependencyUtlilizationService: DependencyUtlilizationService;
  const listOfAPIs: DomainAPIOptions[] = [
    { key: 'domain_api', url: 'http://localhost:3001' },
  ];

  const apiHealthStatus = {
    status: HealthStatus.fail,
    type: ComponentType.url,
    componentId: 'domain_api',
    description: `Health Status of $http://localhost:3001 is: fail`,
    time: Date.now(),
    output: '',
    links: {},
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ApiHealthIndicator,
        {
          provide: HttpService,
          useValue: new HttpService(),
        },
        {
          provide: DependencyUtlilizationService,
          useValue: new DependencyUtlilizationService(),
        },
      ],
    }).compile();

    apiHealthIndicator = module.get<ApiHealthIndicator>(ApiHealthIndicator);

    httpService = module.get<HttpService>(HttpService);
    dependencyUtlilizationService = module.get<DependencyUtlilizationService>(
      DependencyUtlilizationService,
    );
  });

  it('should be defined', () => {
    expect(apiHealthIndicator).toBeDefined();
  });

  it('isHealthy should return status as true when pingCheck return true', () => {
    jest
      .spyOn(dependencyUtlilizationService, 'isRecentlyUsed')
      .mockReturnValue(true);

    const result = apiHealthIndicator.isHealthy(listOfAPIs);

    result.then(response => {
      expect(response['domain_api'].status).toBe(HealthStatus.pass);
    });
  });
  it('isHealthy should return status as false when pingCheck return false', () => {
    jest
      .spyOn(dependencyUtlilizationService, 'isRecentlyUsed')
      .mockReturnValue(false);

    jest.spyOn(httpService, 'request').mockImplementation(config => {
      throw new Error('could not call api');
    });

    const result = apiHealthIndicator.isHealthy(listOfAPIs);

    result
      .then(response => {
        expect(response['domain_api'].status).toBe(HealthStatus.fail);
      })
      .catch(reject => {
        expect(reject['domain_api'].status).toBe(HealthStatus.fail);
        expect(reject).toBe(apiHealthStatus);
      });
  });
});
