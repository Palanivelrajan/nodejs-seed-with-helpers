import { HttpService, Injectable } from '@nestjs/common';
import { DependencyUtlilizationService } from '../dependency-utlilization/dependency-utlilization.service';
import { ComponentType } from '../enums/component-type.enum';
import { HealthStatus } from '../enums/health-status.enum';
import { ComponentHealthCheckResult } from '../interfaces/component-health-check-result.interface';
import { ApiHealthCheckOptions } from './interfaces/api-health-check-options.interface';
import { DomainAPIOptions } from '../../domain-api';
import { catchError } from 'rxjs/operators';

@Injectable()
export class ApiHealthIndicator {
  private healthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  };
  constructor(
    private readonly httpService: HttpService,
    private readonly dependencyUtilizationService: DependencyUtlilizationService,
  ) {
    this.healthIndicatorResponse = {};
  }

  private async pingCheck(api: DomainAPIOptions) {
    return (
      this.dependencyUtilizationService.isRecentlyUsed(api.key) ||
      (await this.httpService.request({ url: api.url }).toPromise())
    );
  }

  async isHealthy(
    listOfAPIs: DomainAPIOptions[],
  ): Promise<{ [key: string]: ComponentHealthCheckResult }> {
    for (const api of listOfAPIs) {
      const apiHealthStatus = {
        status: HealthStatus.fail,
        type: ComponentType.url,
        componentId: api.key,
        description: `Health Status of ${api.url} is: fail`,
        time: Date.now(),
        output: '',
        links: {},
      };
      await this.pingCheck(api)
        .then(response => {
          apiHealthStatus.status = HealthStatus.pass;
          apiHealthStatus.description = `Health Status of ${api.url} is: pass`;
          this.healthIndicatorResponse[api.key] = apiHealthStatus;
        })
        .catch(rejected => {
          this.healthIndicatorResponse[api.key] = apiHealthStatus;
        });
    }
    return this.healthIndicatorResponse;
  }
}
