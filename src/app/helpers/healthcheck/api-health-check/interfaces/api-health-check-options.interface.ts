export interface ApiHealthCheckOptions {
  name: string;
  key: string;
  url: string;
}
