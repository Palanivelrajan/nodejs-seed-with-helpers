import { Injectable } from '@nestjs/common';
import { HealthCheckResult } from '../healthcheck/interfaces/health-check-result.interface';
import { ApiHealthIndicator } from './api-health-check/api-health-indicator';
import { HealthStatus } from './enums/health-status.enum';
import { PostgressdbHealthCheckService } from './postgressdb-health-check/service/postgressDB-health-check.service';
import { OracledbHealthCheckService } from './oracledb-health-check/service/oracledb-health-check.service';
import { ConfigService } from '../config';

@Injectable()
export class HealthCheckService {
  private healthCheckResult: HealthCheckResult;
  constructor(
    private readonly config: ConfigService,
    private readonly apiHealthIndicator: ApiHealthIndicator,
    private readonly postgressdbHealthCheckService: PostgressdbHealthCheckService,
    private readonly oracledbHealthCheckService: OracledbHealthCheckService,
  ) {}

  async isHealthy(): Promise<HealthCheckResult> {
    let isHealthy = HealthStatus.pass;
    const apiHealthCheckResult = await this.apiHealthIndicator.isHealthy(
      this.config.domainAPIOptions,
    );
    for (const key in apiHealthCheckResult) {
      if (apiHealthCheckResult[key].status === HealthStatus.fail) {
        isHealthy = HealthStatus.fail;
        break;
      } else if (apiHealthCheckResult[key].status === HealthStatus.warn) {
        isHealthy = HealthStatus.warn;
      }
    }

    const postgressDBHealthCheckResult = await this.postgressdbHealthCheckService.isHealthy(
      this.config.databaseOptions.postgressDB,
    );

    for (const key in postgressDBHealthCheckResult) {
      if (postgressDBHealthCheckResult[key].status === HealthStatus.fail) {
        isHealthy = HealthStatus.fail;
        break;
      } else if (
        postgressDBHealthCheckResult[key].status === HealthStatus.warn
      ) {
        isHealthy = HealthStatus.warn;
      }
    }

    const oracleDBHealthCheckResult = await this.oracledbHealthCheckService.isHealthy(
      this.config.databaseOptions.oracleDB,
    );

    for (const key in oracleDBHealthCheckResult) {
      if (oracleDBHealthCheckResult[key].status === HealthStatus.fail) {
        isHealthy = HealthStatus.fail;
        break;
      } else if (oracleDBHealthCheckResult[key].status === HealthStatus.warn) {
        isHealthy = HealthStatus.warn;
      }
    }

    this.healthCheckResult = {
      status: isHealthy,
      version: 'v1.0.0',
      releaseID: 'release1',
      notes: [' '],
      output: ' ',
      serviceID: '123123',
      description: `Health status of the BFF application is ${isHealthy}.`,
      details: [
        apiHealthCheckResult,
        oracleDBHealthCheckResult,
        postgressDBHealthCheckResult,
      ],
      links: {},
    };
    return this.healthCheckResult;
  }
}
