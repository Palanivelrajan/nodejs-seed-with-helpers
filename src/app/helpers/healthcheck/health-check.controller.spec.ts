import { HttpService, HttpModule } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { ApiHealthIndicator } from './api-health-check/api-health-indicator';
import { DependencyUtlilizationService } from './dependency-utlilization/dependency-utlilization.service';
import { HealthCheckController } from './health-check.controller';
import { HealthCheckService } from './health-check.service';
import { DatabaseHealthCheckOptions } from './interfaces/database-health-check-options.interface';
import { PostgressDBService } from '../database/postgress-db/service/postgress-db.service';
import { PostgressdbHealthCheckService } from './postgressdb-health-check/service/postgressDB-health-check.service';
import { OracledbHealthCheckService } from './oracledb-health-check/service/oracledb-health-check.service';
import { OracleDBService } from '../database/oracledb';
import { HealthStatus } from './enums/health-status.enum';
import { ComponentType } from './enums/component-type.enum';
import { ConfigModule } from '../config';
import { configure } from 'winston';
import { ComponentHealthCheckResult } from './interfaces/component-health-check-result.interface';
import { HealthCheckResult } from './interfaces/health-check-result.interface';
import { AppLoggerService } from '../logger';
describe('HealthCheck Controller', () => {
  let healthCheckController: HealthCheckController;
  let healthCheckService: HealthCheckService;
  let healthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  } = {
    domain_api: {
      status: HealthStatus.warn,
      type: ComponentType.url,
      componentId: 'domain_api',
      description: `Health Status of http://localhost:3001 is: fail`,
      time: Date.now(),
      output: '',
      links: {},
    },
  };

  let postgressHealthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  } = {
    shipperanalyticsq: {
      status: HealthStatus.warn,
      type: ComponentType.database,
      componentId: 'shipperanalyticsq',
      description: `Health Status of is: fail`,
      time: Date.now(),
      output: '',
      links: {},
    },
  };

  let oracleHealthIndicatorResponse: {
    [key: string]: ComponentHealthCheckResult;
  } = {
    rfdest: {
      status: HealthStatus.warn,
      type: ComponentType.database,
      componentId: '',
      description: `Health Status of  is: fail`,
      time: Date.now(),
      output: '',
      links: {},
    },
  };
  let healthCheckResult: HealthCheckResult = {
    status: HealthStatus.warn,
    version: 'v1.0.0',
    releaseID: 'release1',
    notes: [' '],
    output: ' ',
    serviceID: '123123',
    description: `Health status of the BFF application is ${HealthStatus.warn}.`,
    details: [
      healthIndicatorResponse,
      oracleHealthIndicatorResponse,
      postgressHealthIndicatorResponse,
    ],
    links: {},
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule, ConfigModule.forRoot({ folder: '/src/config' })],
      controllers: [HealthCheckController],
      providers: [
        HealthCheckService,
        ApiHealthIndicator,
        PostgressdbHealthCheckService,
        OracledbHealthCheckService,
        DependencyUtlilizationService,
        PostgressDBService,
        OracleDBService,
        AppLoggerService,
      ],
    }).compile();

    healthCheckController = module.get<HealthCheckController>(
      HealthCheckController,
    );
    healthCheckService = module.get<HealthCheckService>(HealthCheckService);
  });

  it('should be defined', () => {
    expect(healthCheckController).toBeDefined();
  });

  it('call get (ie. health endpoint)', () => {
    jest
      .spyOn(healthCheckService, 'isHealthy')
      .mockReturnValue(Promise.resolve(healthCheckResult));
    healthCheckController.get().then(response => {
      expect(response).toEqual(healthCheckResult);
    });
  });
});
