import { OracleDBHealthCheckOptions } from '../oracledb-health-check/interface/oracledb-health-check-options.interface';
import { PostgressDBHealthCheckOptions } from '../postgressdb-health-check/interface/postgressdb-health-check-options.interface';

export interface DatabaseHealthCheckOptions {
  oracleDBOptions: OracleDBHealthCheckOptions[];
  postgressDBOption: PostgressDBHealthCheckOptions[];
}
