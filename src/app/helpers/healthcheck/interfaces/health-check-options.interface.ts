import { ApiHealthCheckOptions } from '../api-health-check/interfaces/api-health-check-options.interface';
import { DatabaseHealthCheckOptions } from './database-health-check-options.interface';
export interface HealthCheckOptions {
  id: string;
  dnsList: ApiHealthCheckOptions[];
  databaseList: DatabaseHealthCheckOptions;
}
