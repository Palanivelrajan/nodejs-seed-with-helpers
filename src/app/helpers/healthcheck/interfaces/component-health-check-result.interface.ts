import { ComponentType } from '../enums/component-type.enum';
import { HealthStatus } from '../enums/health-status.enum';
export interface ComponentHealthCheckResult {
  status: HealthStatus;
  componentId: string;
  type: ComponentType;
  description: string;
  time: number;
  links: any;
  output: any;
}
