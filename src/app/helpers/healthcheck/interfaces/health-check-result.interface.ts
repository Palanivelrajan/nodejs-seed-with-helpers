import { HealthStatus } from '../enums/health-status.enum';
import { ComponentHealthCheckResult } from './component-health-check-result.interface';
export interface HealthCheckResult {
  status: HealthStatus;
  version: string;
  releaseID: string;
  notes: [string];
  output: string;
  serviceID: string;
  description: string;
  details: Array<{ [name: string]: ComponentHealthCheckResult }>;
  links: { [name: string]: string };
}
