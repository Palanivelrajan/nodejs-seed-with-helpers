import { DynamicModule, Global, Module } from '@nestjs/common';
import { LoggerModule } from '../logger/logger.module';
import { LoggingInterceptor } from './logger/logging.interceptor';
import { TimeoutInterceptor } from './timeout/timeout.interceptor';
import { TransformInterceptor } from './transform/transform.interceptor';

@Global()
@Module({})
export class InterceptorsModule {
  static forRoot(): DynamicModule {
    return {
      module: InterceptorsModule,
      imports: [LoggerModule],
      providers: [TransformInterceptor, LoggingInterceptor, TimeoutInterceptor],
      exports: [],
    };
  }
}
