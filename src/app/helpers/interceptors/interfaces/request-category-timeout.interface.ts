export interface RequestCategoryTimeout {
  category: string;
  timeout: number;
}
