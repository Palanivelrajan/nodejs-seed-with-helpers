import { LoggingInterceptor } from './logging.interceptor';
import { of } from 'rxjs';

const mockLogger = { verbose: jest.fn() }
const returnResponse = { name: 'This is testing' };

const mockCallHandler: any = {
  handle: () => of(returnResponse),
};

const mockContext: any = {
  switchToHttp: () => ({
    getRequest: () => ({
      raw: () => {
        method: () => (jest.fn().mockReturnValue("POST"))
        url: "mock-url"
      }
    }),
    getResponse: () => {
      const response = {
        code: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        }
      };
      return response;
    }
  })
};

describe('LoggingInterceptor', () => {
  let interceptor: LoggingInterceptor;

  beforeEach(() => {
    interceptor = new LoggingInterceptor(mockLogger as any);
  });

  // it(' call Handle and  log the error', () => {
  //   interceptor.intercept(mockContext, mockCallHandler);
  //   expect(mockLogger.verbose).toBeCalled();

  it('should successfully return', (done) => {
    // if your interceptor has logic that depends on the context
    // you can always pass in a mock value instead of an empty object
    // just make sure to mock the expected alls like switchToHttp
    // and getRequest
    interceptor.intercept(mockContext, mockCallHandler).subscribe({
      next: (value) => {
        expect(mockLogger.verbose).toBeCalled();
      },
      error: (error) => {
        throw error;
      },
      complete: () => {
        done();
      },
    });
  });

  //});

});