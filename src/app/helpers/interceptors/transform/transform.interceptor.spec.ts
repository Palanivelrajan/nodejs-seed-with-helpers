import { TransformInterceptor } from './transform.interceptor';
import { AppLoggerService } from './../../logger/services/app.logger.service';
import { TestingModule, Test } from '@nestjs/testing';
import { of } from 'rxjs';

const mockLogger = { verbose: jest.fn() };
const returnResponse = { name: 'This is testing' };
const mockCallHandler: any = {
  handle: () => of(returnResponse),
};

const mockContext: any = {
  getHandler: () => {
    return 'fetchOracleQueryData';
  },
  switchToHttp: () => ({
    getRequest: () => ({
      raw: () => {
        method: () => jest.fn().mockReturnValue('POST');
        url: 'mock-url';
      },
    }),
    getResponse: () => {
      const response = {
        code: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        },
      };
      return response;
    },
  }),
};

describe('TransformInterceptor', () => {
  let transformInterceptor: TransformInterceptor;
  let appLoggerService: AppLoggerService;
  beforeEach(async () => {
    const loggerOptions = { appPath: process.cwd() };
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransformInterceptor,
        {
          provide: AppLoggerService,
          useValue: new AppLoggerService(),
        },
      ],
    }).compile();
    transformInterceptor = module.get<TransformInterceptor>(
      TransformInterceptor,
    );
  });

  it('should be defined', () => {
    expect(transformInterceptor).toBeDefined();
  });

  it('should successfully return', done => {
    // if your interceptor has logic that depends on the context
    // you can always pass in a mock value instead of an empty object
    // just make sure to mock the expected alls like switchToHttp
    // and getRequest

    transformInterceptor.intercept(mockContext, mockCallHandler).subscribe({
      next: value => {
        expect(value).toBeTruthy();
      },
      error: error => {
        throw error;
      },
      complete: () => {
        done();
      },
    });
  });
});
