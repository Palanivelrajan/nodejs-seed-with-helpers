import { ConfigOptions } from './config/interfaces/config-options.interface';
import { OracleDBOptions } from './database/oracledb';
import { PostgressDBOptions } from './database/postgress-db';
import { DomainAPIOptions } from './domain-api';
import { FilterOptions } from './filters';
import { HealthCheckOptions } from './healthcheck/interfaces/health-check-options.interface';
import { InterceptorOptions } from './interceptors';
import { LoggerOptions } from './logger';
import { RabbitMQOptions } from './rabbitmq';
export interface SynapseBFFCoreOptions {
  configOptions: ConfigOptions;
}
