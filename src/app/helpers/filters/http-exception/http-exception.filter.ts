import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { AppLoggerService } from '../../logger';
@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private logger: AppLoggerService) {}

  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    let status;

    if (exception.getStatus) {
      status = exception.getStatus();
    }
    const message = {
      Title: exception.message.error,
      Type: 'Exception - HttpExceptionFilter',
      Detail: exception.message,
      Status: '',
    };

    this.logger.error(message, exception.stack);

    response.code(status).send({
      statusCode: status,
      ...(exception.getResponse() as object),
      timestamp: 'Exception - HttpExceptionFilter' + new Date().toISOString(),
    });
  }
}
