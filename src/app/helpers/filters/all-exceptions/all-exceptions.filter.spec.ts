import { AllExceptionsFilter } from './all-exceptions.filter';
import { TestingModule, Test } from '@nestjs/testing';
import { HttpException } from '@nestjs/common';

const mockLogger = { error: jest.fn() };

const mockContext: any = {
  switchToHttp: () => ({
    getRequest: () => ({
      url: 'mock-url',
    }),
    getResponse: () => {
      const response = {
        code: 0,
        status: code => {
          response.code = code;
          return response;
        },
        send: data => {
          return data;
        },
      };
      return response;
    },
  }),
};

describe('AllExceptionFilter', () => {
  let filter: AllExceptionsFilter;
  beforeEach(() => {
    filter = new AllExceptionsFilter(mockLogger as any);
  });
  it('should catch and log the error', () => {
    const mockException = {
      name: 'unknown',
      message: 'This message details',
    };
    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });

  it('should catch and log the error', () => {
    const mockException: HttpException = new HttpException(
      'RequestTimeoutExceptionFilter',
      404,
    );
    filter.catch(mockException, mockContext);
    expect(mockLogger.error).toBeCalled();
  });
});
